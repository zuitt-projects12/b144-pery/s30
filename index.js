const express = require("express");
// Mongoose is a package that allows creation of schemas to model our data structures and to have an access to a number of methods for manipulating our database.
const mongoose = require("mongoose");

const app = express();
const port = 3001;

// MongoDB connection
// Connecting to MongoDB Atlas
mongoose.connect("mongodb+srv://admin:admin@wdc028-course-booking.dwfxo.mongodb.net/batch144-to-do?retryWrites=true&w=majority", {
	useNewUrlParser: true,
	useUnifiedTopology: true
});

// Set notification for connection success or failure
let db = mongoose.connection;
// If error occured, output in the console.
db.on("error", console.error.bind(console, "connection error"));
// If the connection is successful, output in the console.
db.once("open", () => console.log("We're connected to the cloud database."));

app.use(express.json());
app.use(express.urlencoded({ extended:true }));

// Mongoose Schemas
/*
	Schemas determine the structure of the documents to be written in the database. It acts as a blueprint to our data.

	We will use Schema() constructor of the Mongoose module to create a new Schema object.
*/

const taskSchema = new mongoose.Schema({
	name: String,
	status: {
		type: String,
		// Default values are the predefine values for a field if we don't put any value
		default: "pending" 
	}
});
const userSchema = new mongoose.Schema({
	username: String,
	password: String
});

const Task = mongoose.model("Task", taskSchema);
const User = mongoose.model("User", userSchema);
// Models are what allows us to gain access to methods that will perform CRUD functions.
// Models must be in singular form and Capitalized following the MVC approach for naming conventions.
// firstParameter of the mongoose.model() indicates the collection in where to store the data.
// secondParameter is used to specify the Schema/blueprint.

// Routes

// Create a new task
/*
	Business logic
	1. Add a functionality to check if there are duplicate tasks.
		- If the task already exist, return "there is a duplicate".
		- If the task doesn't exist, we can add it in the database.
	2. The task data will be coming from the request body.
	3. Create a new Task object with properties that we need.
	4. Then save the data.
*/

app.post("/tasks", (req, res) => {
	// Check if there are duplicates tasks
	// findOne() is a mongoose method that acts similar to "find". It returns the first document that matches the search criteria.
	Task.findOne( {name: req.body.name}, (error, result) => {
		// If a document was found and the document's name matches the information sent via client/Postman
		if(result !== null && result.name === req.body.name){
			// return the message to the client / postman
			return res.send("Duplicate task found");
		}
		else{
			// If no document was found 
			// Create a new Task and save it to the database
			let newTask = new Task({
				name: req.body.name
			}); 
			newTask.save((saveErr, savedTask) => {
				// If there are errors in saving
				if(saveErr){
					// Will print any errors found in the console
					// saveErr is an error abject that will contain details about the error
					// Errors normally come as an object data type.
					return console.error(saveErr);
				}
				else{
					// No error found while creating the document.
					// Return a status code of 201 for successful creation
					return res.status(201).send("New Task Created");
				} // ./ else saveErr
			}); // ./newTask.save()
		} // ./else result !== null && result.name === req.body.name
	} ); // ./Task.findOne()
}); // ./app.post("/tasks"...)

/*
	Business logic for retrieving all data.
	1. Retrieve all the documents using the find().
	2. If an error is encountered, print the error.
	3. If no errors are found, send a success status back to client and return an array of documents. 
*/

app.get("/tasks", (req, res) => {
	// An empty "{}" means it returns all the documents and stores them in the "results" parameter of the call back function
	Task.find( {}, (err, result) => {
		// If an error occured
		if(err){
			return console.log(err);
		}
		else{
			return res.status(200).json({
				data: result
			});
		} // else err
	}); // Task.find()
}); //app.get("/users")

/*
Register a user(Business Logic)
1. Find if there are duplicate user
	-If user already exists, we return an error
	-if user doesn't exist, we add it in the database
		-if the username and password are both not blank
			-if blank, send response "BOTH username and password must be provided"
			- both condition has been met, create a new object.
			-Save the new object
				-if error, return an error message
				-else, response a status 201 and "New User Registered"
*/
/*
	Business logic for retrieving all data.
	1. Retrieve all the documents using the find().
	2. If an error is encountered, print the error.
	3. If no errors are found, send a success status back to client and return an array of documents. 
*/

app.post("/signup", (req, res) => {
	User.findOne( {username: req.body.username}, (err, result) => {
		if(result !== null && result.username === req.body.username){
			return res.send(`There is a user that uses "${result.username}"`);
		}
		else{
			if(req.body.username === "" || req.body.password === ""){
				return res.send("BOTH username and password must be provided.");
			}
			else{
				let newUser = new User({
					username: req.body.username,
					password: req.body.password
				});
				newUser.save((saveErr, savedUser) => {
					if(saveErr){
						return console.error(saveErr);
					}
					else{
						return res.status(201).send(`Username ${req.body.username} created.`);
					} // else (saveErr)
				}); // newUser.save
			}
		} // else (result)
	}); // User.findOne()
}); // app.post("/signup")

app.get("/users", (req,res) => {
	User.find({}, (err, result) => {
		if(err){
			return console.error(err);
		}
		else{
			return res.status(200).json({
				data: result
			});
		} // else
	}); // User.find({})
}); // app.get("/users")

app.listen(port, () => console.log(`Server is running at port ${port}.`));